<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipoPago extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_pagos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion')->nullable();
        });

        DB::table('tipo_pagos')->insert(array('descripcion'=>'contado'));
        DB::table('tipo_pagos')->insert(array('descripcion'=>'credito'));



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
