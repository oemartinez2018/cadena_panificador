<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentasAbonosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ventas_abonos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idabono')->unsigned();
            $table->integer('idventa')->unsigned();
            $table->integer('iduser')->unsigned();
            $table->date('fecha_hora');
            $table->text('descripcion');
            $table->decimal('abono', 11, 2);

            $table->foreign('idventa')->references('id')->on('ventas')->onDelete('cascade');
            $table->foreign('iduser')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('idabono')->references('id')->on('abonos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventas_abonos');
    }
}
