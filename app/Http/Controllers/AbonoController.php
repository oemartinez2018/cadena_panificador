<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Venta;
use App\User;
use App\Abono;
use App\VentaAbono;
use Carbon\Carbon;

class AbonoController extends Controller
{
    

    public function historial_abonos(Request $request)
    {
        //if (!$request->ajax()) return redirect('/');

        $buscar = $request->buscar;

        $abonos =Venta::join('ventas_abonos','ventas_abonos.idventa','=','ventas.id')
            ->join('abonos','abonos.id','=','ventas_abonos.idabono')
            ->where('ventas.idcliente','=',$request->idcliente)
            ->where('ventas.tipo_pago','=',$request->idtipopago)
            ->where('ventas.id','=',$request->idventa)
            ->select('ventas.tipo_comprobante','ventas.serie_comprobante','ventas.num_comprobante',
                    'ventas.fecha_hora as fecha_venta','ventas.total','ventas.saldo',
                    'abonos.id as idabono','abonos.numnota',
                    'ventas_abonos.fecha_hora as fechaabono',
                    'ventas_abonos.descripcion','ventas_abonos.abono')          
            ->orderBy('abonos.id', 'desc')->paginate(10);
        
        return [
            'pagination' => [
                'total'        => $abonos->total(),
                'current_page' => $abonos->currentPage(),
                'per_page'     => $abonos->perPage(),
                'last_page'    => $abonos->lastPage(),
                'from'         => $abonos->firstItem(),
                'to'           => $abonos->lastItem(),
            ],
            'abonos' => $abonos
        ];
    }


    //mostrando las facturas que debera cancelar
      public function index(Request $request)
    {
        //if (!$request->ajax()) return redirect('/');

        $buscar = $request->buscar;
        $criterio = $request->criterio;
        $tabla = $request->campo;
        
            $ventas = Venta::join('personas','ventas.idcliente','=','personas.id')
            ->join('users','ventas.idusuario','=','users.id')
            ->join('tipo_pagos','ventas.tipo_pago','=','tipo_pagos.id')
            ->select('ventas.id','ventas.tipo_comprobante','ventas.serie_comprobante',
            'ventas.num_comprobante','ventas.fecha_hora','ventas.impuesto','ventas.total',
            'ventas.estado','ventas.saldo','personas.nombre','users.usuario','tipo_pagos.id as idtipopago','tipo_pagos.descripcion as condicion','ventas.idcliente as idcliente')
            ->where('ventas.tipo_pago','=',2)
            ->where($tabla.'.'.$criterio, 'like', '%'. $buscar . '%')
            ->where('ventas.saldo', '>','0')
            ->orderBy('ventas.fecha_hora', 'desc')->paginate(10);
       
        
        return [
            'pagination' => [
                'total'        => $ventas->total(),
                'current_page' => $ventas->currentPage(),
                'per_page'     => $ventas->perPage(),
                'last_page'    => $ventas->lastPage(),
                'from'         => $ventas->firstItem(),
                'to'           => $ventas->lastItem(),
            ],
            'ventas' => $ventas
        ];
    }



     public function store(Request $request)
    {
       // if (!$request->ajax()) return redirect('/');

        try{
            DB::beginTransaction();

            $mytime= Carbon::now('America/Lima');

            $abono = new Abono();
            $abono->numnota=$request->numabono;
            $abono->save();

            $detalles = $request->data;//Array de detalles
            //Recorro todos los elementos

            foreach($detalles as $ep=>$det)
            {
                $detalle = new VentaAbono();
                $detalle->idabono = $abono->id;
                $detalle->idventa = $det['idventa'];
                $detalle->iduser = \Auth::user()->id;
                $detalle->fecha_hora = $mytime->toDateString();
                $detalle->descripcion = $det['descripcion'];
                $detalle->abono = $det['abono'];      
                $detalle->save();

                //buscando la venta para realizar la descarga
                $venta = Venta::FindOrFail($detalle->idventa);
                $venta->saldo = $venta->saldo - $detalle->abono;
                $venta->save(); 
            }       
            DB::commit();
            return [
                'id' => $abono->id,
            ];
        } catch (Exception $e){
            DB::rollBack();
        }
    }

      public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $abono = Abono::findOrFail($request->id);
        $abono->estado = 'Anulado';
        $abono->save();
    }
}
