<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Venta;
use Carbon\Carbon;
use App\Articulo;
use App\DetalleIngreso;
use App\Ingreso;
use DB;

class IvaController extends Controller
{
    public function CalIvaVenta(Request $request)
    {

        $finicio = Carbon::parse($request->finicio)->format('Y-m-d');
        $ffinal= Carbon::parse($request->ffinal)->format('Y-m-d');

        $ivaPagar = DB::table("ventas")
            ->select(DB::raw('sum(total-(FORMAT(total/1.13,2))) as total'))
            ->where("estado",'=',"Registrado")
            ->whereBetween('fecha_hora', [$finicio, $ffinal])
            ->get();

        /*$facturas = Venta::whereBetween('fecha_hora', [$finicio, $ffinal])
        ->where("estado",'=',"Registrado")
        ->sum("total");*/

        return [
            "facturas" => round($ivaPagar[0]->total,2)
            
        ];
    }

    //public function articulos ventas
     public function venta_iva(Request $request)
    {
       // if (!$request->ajax()) return redirect('/');

        $buscar = $request->buscar;
        $criterio = $request->criterio;
        $finicio = Carbon::parse($request->finicio)->format('Y-m-d');
        $ffinal= Carbon::parse($request->ffinal)->format('Y-m-d');
        


        $articulos = Venta::whereBetween('fecha_hora', [$finicio, $ffinal])
         ->select('id','tipo_comprobante','num_comprobante',
                            'serie_comprobante','fecha_hora',
                                'impuesto','total','estado',
                                DB::raw('FORMAT(total-(total/1.13),2) iva_retenido'),
                                DB::raw('FORMAT(total/1.13,2) as sumas')
                                )
                     ->groupBy('ventas.id')
                    ->orderBy('ventas.fecha_hora', 'desc')->paginate(10);
           


        /*if ($buscar==''){
            $articulos = Articulo::join('detalle_ventas','detalle_ventas.idarticulo','=','articulos.id')
            ->join('ventas','ventas.id','=','detalle_ventas.idventa')
            ->select('articulos.codigo','articulos.nombre','articulos.precio_venta',
                    'ventas.tipo_comprobante','ventas.serie_comprobante',
                    'ventas.num_comprobante','ventas.fecha_hora','ventas.impuesto','ventas.total')
             ->whereBetween('fecha_hora', [$finicio, $ffinal])
            ->orderBy('ventas.fecha_hora', 'desc')->paginate(10);
        }
        else{
           $articulos = Articulo::join('detalle_ventas','detalle_ventas.idarticulo','=','articulos.id')
            ->join('ventas','ventas.id','=','detalle_ventas.idventa')
            ->select('articulos.codigo','articulos.nombre','articulos.precio_venta',
                    'ventas.tipo_comprobante','ventas.serie_comprobante',
                    'ventas.num_comprobante','ventas.fecha_hora','ventas.impuesto','ventas.total')
            ->where('ventas.'.$criterio, 'like', '%'. $buscar . '%')
             ->whereBetween('fecha_hora', [$finicio, $ffinal])
            ->orderBy('ventas.fecha_hora', 'desc')->paginate(10);
        }*/
        
        return [
            'pagination' => [
                'total'        => $articulos->total(),
                'current_page' => $articulos->currentPage(),
                'per_page'     => $articulos->perPage(),
                'last_page'    => $articulos->lastPage(),
                'from'         => $articulos->firstItem(),
                'to'           => $articulos->lastItem(),
            ],
            'articulos' => $articulos
        ];
    }

    //iva para las compras

      public function CalIvaIngresos(Request $request)
    {

        $finicio = Carbon::parse($request->finicio)->format('Y-m-d');
        $ffinal= Carbon::parse($request->ffinal)->format('Y-m-d');

        $ivaPagar = DB::table("ingresos")
            ->select(DB::raw('sum(total-(FORMAT(total/1.13,2))) as total'))
            ->where("estado",'=',"Registrado")
            ->whereBetween('fecha_hora', [$finicio, $ffinal])
            ->get();

        /*$facturas = Venta::whereBetween('fecha_hora', [$finicio, $ffinal])
        ->where("estado",'=',"Registrado")
        ->sum("total");*/

        return [
            "facturas" => round($ivaPagar[0]->total,2)
            
        ];
    }

     public function ingreso_iva(Request $request)
    {
       // if (!$request->ajax()) return redirect('/');

        $buscar = $request->buscar;
        $criterio = $request->criterio;
        $finicio = Carbon::parse($request->finicio)->format('Y-m-d');
        $ffinal= Carbon::parse($request->ffinal)->format('Y-m-d');
        


        $articulos = Ingreso::whereBetween('fecha_hora', [$finicio, $ffinal])
                    ->select('id','tipo_comprobante',
                            'serie_comprobante','fecha_hora','num_comprobante',
                                'impuesto','total','estado',
                                DB::raw('FORMAT(total-(total/1.13),2) iva_retenido'),
                                DB::raw('FORMAT(total/1.13,2) as sumas'))
                     ->groupBy('ingresos.id')
                    ->orderBy('ingresos.fecha_hora', 'desc')->paginate(10);


        /*if ($buscar==''){
            $articulos = Articulo::join('detalle_ventas','detalle_ventas.idarticulo','=','articulos.id')
            ->join('ventas','ventas.id','=','detalle_ventas.idventa')
            ->select('articulos.codigo','articulos.nombre','articulos.precio_venta',
                    'ventas.tipo_comprobante','ventas.serie_comprobante',
                    'ventas.num_comprobante','ventas.fecha_hora','ventas.impuesto','ventas.total')
             ->whereBetween('fecha_hora', [$finicio, $ffinal])
            ->orderBy('ventas.fecha_hora', 'desc')->paginate(10);
        }
        else{
           $articulos = Articulo::join('detalle_ventas','detalle_ventas.idarticulo','=','articulos.id')
            ->join('ventas','ventas.id','=','detalle_ventas.idventa')
            ->select('articulos.codigo','articulos.nombre','articulos.precio_venta',
                    'ventas.tipo_comprobante','ventas.serie_comprobante',
                    'ventas.num_comprobante','ventas.fecha_hora','ventas.impuesto','ventas.total')
            ->where('ventas.'.$criterio, 'like', '%'. $buscar . '%')
             ->whereBetween('fecha_hora', [$finicio, $ffinal])
            ->orderBy('ventas.fecha_hora', 'desc')->paginate(10);
        }*/
        
        return [
            'pagination' => [
                'total'        => $articulos->total(),
                'current_page' => $articulos->currentPage(),
                'per_page'     => $articulos->perPage(),
                'last_page'    => $articulos->lastPage(),
                'from'         => $articulos->firstItem(),
                'to'           => $articulos->lastItem(),
            ],
            'articulos' => $articulos
        ];
    }

}
