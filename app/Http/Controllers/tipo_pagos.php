<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TipoPago;
class tipo_pagos extends Controller
{
    public function index(Request $request)
    {
        //if (!$request->ajax()) return redirect('/');

        $tipos = TipoPago::select('id','descripcion')->get();
        return [
            "tipos" => $tipos
        ];
    }
}
