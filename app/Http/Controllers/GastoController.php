<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Gasto;
use App\DetalleGasto;
class GastoController extends Controller
{


    public function index(Request $request)
    {
       // if (!$request->ajax()) return redirect('/');

        $opcion = $request->opcion;
        //$buscar = $request->buscar;
        $criterio = $request->criterio;
        
        if ($opcion==0){
            $gastos = Gasto::join('personas','gastos.idproveedor','=','personas.id')
            ->join('users','gastos.idusuario','=','users.id')
            ->select('gastos.id','gastos.tipo_comprobante','gastos.serie_comprobante',
            'gastos.num_comprobante','gastos.fecha_hora','gastos.impuesto','gastos.total',
            'gastos.estado','personas.nombre','users.usuario')
            ->orderBy('gastos.id', 'desc')->paginate(10);
        }
        else{

            switch($opcion)
            {
                case 1: //que solo busque por comprobante
                     $gastos = Gasto::join('personas','gastos.idproveedor','=','personas.id')
                    ->join('users','gastos.idusuario','=','users.id')
                    ->select('gastos.id','gastos.tipo_comprobante','gastos.serie_comprobante',
                    'gastos.num_comprobante','gastos.fecha_hora','gastos.impuesto','gastos.total',
                    'gastos.estado','personas.nombre','users.usuario')
                    ->where('gastos.'.$criterio, 'like', '%'. $request->buscar . '%')
                    ->orderBy('gastos.id', 'desc')->paginate(10);
                break;

                case 2: //que busque por fecha
                    $finicio = Carbon::parse($request->fechaI)->format('Y-m-d');
                    $ffinal= Carbon::parse($request->fechaF)->format('Y-m-d');
                    $gastos = Gasto::join('personas','gastos.idproveedor','=','personas.id')
                    ->join('users','gastos.idusuario','=','users.id')
                    ->select('gastos.id','gastos.tipo_comprobante','gastos.serie_comprobante',
                    'gastos.num_comprobante','gastos.fecha_hora','gastos.impuesto','gastos.total',
                    'gastos.estado','personas.nombre','users.usuario')
                    ->whereBetween('gastos.fecha_hora', [$finicio, $ffinal])
                    ->orderBy('gastos.id', 'desc')->paginate(10);
                break;

                case 3: //que busque por id cliente
                    
                    $gastos = Gasto::join('personas','gastos.idproveedor','=','personas.id')
                    ->join('users','gastos.idusuario','=','users.id')
                    ->select('gastos.id','gastos.tipo_comprobante','gastos.serie_comprobante',
                    'gastos.num_comprobante','gastos.fecha_hora','gastos.impuesto','gastos.total',
                    'gastos.estado','personas.nombre','users.usuario')
                    ->where('gastos.idproveedor','=',$request->idproveedor)
                    ->orderBy('gastos.id', 'desc')->paginate(10);
                break;
            }
            
        }
        
        return [
            'pagination' => [
                'total'        => $gastos->total(),
                'current_page' => $gastos->currentPage(),
                'per_page'     => $gastos->perPage(),
                'last_page'    => $gastos->lastPage(),
                'from'         => $gastos->firstItem(),
                'to'           => $gastos->lastItem(),
            ],
            'gastos' => $gastos
        ];
    }

    public function obtenerDetalles(Request $request){
        if (!$request->ajax()) return redirect('/');

        $id = $request->id;
        $detalles = DetalleGasto::join('gastos','detalle_gastos.idgasto','=','gastos.id')
        ->select('detalle_gastos.fecha_hora','detalle_gastos.cantidad',
        'detalle_gastos.total','detalle_gastos.precio','detalle_gastos.descripcion')
        ->where('detalle_gastos.idgasto','=',$id)
        ->orderBy('detalle_gastos.id', 'desc')->get();
        
        return ['detalles' => $detalles];
    }


    public function obtenerCabecera(Request $request){
        if (!$request->ajax()) return redirect('/');

        $id = $request->id;
        $gasto = Gasto::join('personas','gastos.idproveedor','=','personas.id')
        ->join('users','gastos.idusuario','=','users.id')
        ->select('gastos.id','gastos.tipo_comprobante','gastos.serie_comprobante',
        'gastos.num_comprobante','gastos.fecha_hora','gastos.impuesto','gastos.total',
        'gastos.estado','personas.nombre','users.usuario')
        ->where('gastos.id','=',$id)
        ->orderBy('gastos.id', 'desc')->take(1)->get();
        
        return ['gasto' => $gasto];
    }



    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        try{
            DB::beginTransaction();

            $mytime= Carbon::now('America/Lima');

            $gasto = new Gasto();
            $gasto->idproveedor = $request->idproveedor;
            $gasto->idusuario = \Auth::user()->id;
            $gasto->tipo_comprobante = $request->tipo_comprobante;
            $gasto->serie_comprobante = $request->serie_comprobante;
            $gasto->num_comprobante = $request->num_comprobante;
            $gasto->fecha_hora = $mytime->toDateString();
            $gasto->impuesto = $request->impuesto;
            $gasto->total = $request->total;
            $gasto->estado = 'Registrado';
        
           
            $gasto->save();

            $detalles = $request->data;//Array de detalles
            //Recorro todos los elementos

            foreach($detalles as $ep=>$det)
            {
                $detalle = new DetalleGasto();
                $detalle->idgasto = $gasto->id;
                $detalle->fecha_hora=$mytime->toDateString();
                $detalle->cantidad = $det['cantidad'];
                $detalle->impuesto = $request->impuesto;
                $detalle->total = $det['total']; 
                $detalle->precio = $det['precio'];  
                $detalle->descripcion = $det['descripcion'];    
                $detalle->estado = 'Registrado'; 
                $detalle->save();
            }       
            DB::commit();
            return [
                'id' => $gasto->id
            ];
        } catch (Exception $e){
            DB::rollBack();
        }    
    }

    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $ingreso = Gasto::findOrFail($request->id);
        $ingreso->estado = 'Anulado';
        $ingreso->save();
    }
}
