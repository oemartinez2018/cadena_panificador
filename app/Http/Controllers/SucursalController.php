<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sucursal;

class SucursalController extends Controller
{
    
    public function index(Request $request)
    {
        //if (!$request->ajax()) return redirect('/');

        $buscar = $request->buscar;
        $criterio = $request->criterio;
        
        if ($buscar==''){
            $sucursales = Sucursal::orderBy('id', 'desc')->paginate(10);
        }
        else{
            $sucursales = Sucursal::where($criterio, 'like', '%'. $buscar . '%')->orderBy('id', 'desc')->paginate(10);
        }
        

        return [
            'pagination' => [
                'total'        => $sucursales->total(),
                'current_page' => $sucursales->currentPage(),
                'per_page'     => $sucursales->perPage(),
                'last_page'    => $sucursales->lastPage(),
                'from'         => $sucursales->firstItem(),
                'to'           => $sucursales->lastItem(),
            ],
            'sucursales' => $sucursales
        ];
    }
    public function selectSucursal(Request $request)
    {
        $sucursales = Sucursal::where('condicion', '=', '1')
        ->select('id','nombre')
        ->orderBy('nombre', 'asc')->get();

        return ['sucursales' => $sucursales];
    }

     public function selectSucursall(Request $request){
        if (!$request->ajax()) return redirect('/');

        $filtro = $request->filtro;
        $sucursales = Sucursal::where('nombre', 'like', '%'. $filtro . '%')
        ->where('condicion','=',1)
        ->select('id','nombre')
        ->orderBy('nombre', 'asc')->get();
        return ['sucursales' => $sucursales];
    }



    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $sucursal = new Sucursal();
        $sucursal->nombre = $request->nombre;
        $sucursal->descripcion = $request->descripcion;
        $sucursal->condicion = '1';
        $sucursal->save();
    }

    public function update(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $sucursal = Sucursal::findOrFail($request->id);
        $sucursal->nombre = $request->nombre;
        $sucursal->descripcion = $request->descripcion;
        $sucursal->condicion = '1';
        $sucursal->save();
    }


    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $sucursal = Sucursal::findOrFail($request->id);
        $sucursal->condicion = '0';
        $sucursal->save();
    }

    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $sucursal = Sucursal::findOrFail($request->id);
        $sucursal->condicion = '1';
        $sucursal->save();
    }


}
