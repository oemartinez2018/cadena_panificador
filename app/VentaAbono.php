<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentaAbono extends Model
{
    protected $table = "ventas_abonos";

    protected $fillable = ["idabono","idventa","iduser","fecha_hora","descripcion","abono"];

    public function abono(){
        return $this->belongsTo('App\Abono');
    }

     public function user(){
        return $this->belongsTo('App\Users');
    }

     public function venta(){
        return $this->belongsTo('App\Venta');
    }
}
