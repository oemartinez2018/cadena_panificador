<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleGasto extends Model
{
    protected $table="detalle_gastos";
    protected $filable=["idgasto","fecha_hora",
                        "cantidad","impuesto",
                        "total", "estado","precio","descripcion"];
    
    public function gasto(){
        return $this->belongsTo('App\Gasto');
    }
}
