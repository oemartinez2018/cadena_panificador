<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gasto extends Model
{
    protected $table="gastos";
    protected $filable=["idproveedor","idusuario",
                        "tipo_comprobante","serie_comprobante",
                        "num_comprobante", "fecha_hora","total","estado"];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function proveedor(){
        return $this->belongsTo('App\Proveedors');
    }

    public function detalle_gastos()
    {
        return $this->hasMany('App\DetalleGasto');
    }
}
