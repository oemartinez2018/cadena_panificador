<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articulo extends Model
{
    protected $fillable =[
        'idcategoria','codigo','nombre','precio_venta','stock','descripcion','condicion','id_sucursal'
    ];
    public function categoria(){
        return $this->belongsTo('App\Categoria');
    }
    public  function sucursal(){
        return $this->belongsTo('App\Sucursal');
    }
}
