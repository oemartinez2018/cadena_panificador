<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    protected $fillable =[
        'idcliente', 
        'idusuario',
        'tipo_comprobante',
        'serie_comprobante',
        'num_comprobante',
        'fecha_hora',
        'impuesto',
        'total',
        'estado',
        'id_sucursal',
        'tipo_documento_2',
        'numero_documento',
        'saldo'
    ];

    public function ventasabonos()
    {
        return $this->hasMany('App\VentaAbono');
    }

     public function tipopago(){
        return $this->belongsTo('App\TipoPago');
    }
}
