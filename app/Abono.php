<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Abono extends Model
{
    protected $table="abonos";

    protected $fillable=["numnota"];

    public function ventasabonos()
    {
        return $this->hasMany('App\VentaAbono');
    }
}
