/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.$ = window.jQuery = require("jquery");

window.Vue = require("vue");

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component("categoria", require("./components/Categoria.vue"));
Vue.component("articulo", require("./components/Articulo.vue"));
Vue.component("cliente", require("./components/Cliente.vue"));
Vue.component("proveedor", require("./components/Proveedor.vue"));
Vue.component("rol", require("./components/Rol.vue"));
Vue.component("sucursales", require("./components/Sucursal.vue"));
Vue.component("user", require("./components/User.vue"));
Vue.component("ingreso", require("./components/Ingreso.vue"));
Vue.component(
  "ingresoprovider",
  require("./components/Almacenero/IngresoProvider.vue")
);
Vue.component("venta", require("./components/Venta.vue"));
Vue.component("ventaseller", require("./components/Vendedor/VentaSeller.vue"));

Vue.component("dashboard", require("./components/Dashboard.vue"));
Vue.component("consultaingreso", require("./components/ConsultaIngreso.vue"));
Vue.component("consultaventa", require("./components/ConsultaVenta.vue"));
Vue.component(
  "consultaventaseller",
  require("./components/Vendedor/ConsultaVentaSeller.vue")
);
Vue.component("ayuda", require("./components/Ayuda.vue"));
Vue.component("acerca", require("./components/Acerca.vue"));

//consulta de iva
Vue.component("iva", require("./components/Iva.vue"));
Vue.component("iva-ventas", require("./components/Iva/Ventas.vue"));
Vue.component("iva-ingresos", require("./components/Iva/Compras.vue"));

//para registrar abonos
Vue.component("abono-ventas", require("./components/Abono/Abono.vue"));
Vue.component("abono-historial", require("./components/Abono/Historial.vue"));

//para registrar los gastos
Vue.component("gasto", require("./components/Gastos/Gasto.vue"));
const app = new Vue({
  el: "#app",
  data: {
    menu: 0,
    ruta:  "http://cadena_panificador.test:8080",
    //ruta: "http://localhost:8000"
   // ruta: "http://cadena_panificador.test:8080"
  }
});
